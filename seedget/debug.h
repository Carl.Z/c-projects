#ifndef DEBUG_INCLUDED 
#define DEBUG_INCLUDED
/* #ifdef NDEBUG
 * #define dputs(...) ((void)0)
 * #define prvar(type, var) ((void)0)
 * #else
 */
#include <stdio.h>
extern int debug;
#define dputs(args...) \
        do { \
                if (debug) { \
        fprintf(stderr, "--- "); \
        fprintf(stderr, args); \
        fprintf(stderr, "\n"); \
        } } while(0)

#define prvar(type, var) \
        dputs("param: %12s: " type, #var, var)
/* #endif                                    
 */
#endif
