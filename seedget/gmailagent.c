/*
 * Send message to mail server
 * Implement send mail to gmail.com
 */
#include <curl/curl.h>
#include <cii/assert.h>
#include <cii/except.h>
#include <cii/mem.h>
#include <cii/arena.h>
#include <string.h>
#include "gmailagent.h"
#include "debug.h"

#define MAXTO 10

#define MIN(a,b) (((a) < (b)) ? (a) : (b))

#define astrdup(a, t, s) \
	do { \
		t = Arena_alloc(a, strlen(s)+1, __FILE__, __LINE__); \
		strcpy(t, s); \
	} while(0)

#define astrcat(a, t, s) \
	do { \
		t = Arena_realloc(a, t, __FILE__, __LINE__); \
		strcat(t, s); \
	} while(0)

struct Except_T GMAILAGENT_FAIL = { "Send mail failed" };

struct GMAILAGENT_T {
	char *server;
	int   port;
	char *username;
	char *password;
	char *from;
	char *to[MAXTO]; /* string array */
	char *message; /* mail body */
	int skip_verification; /* true to skip server cert verification */
	Arena_T arena; /* pre alloc arena to store mail info */
};

GMAILAGENT_T GMAILAGENT_new() {
	GMAILAGENT_T ag;
	ag = NEW0(ag);
	
	ag->arena = Arena_new();
	ag->server = "smtps://smtp.gmail.com";
	ag->port = 465;
	return ag;
}

int GMAILAGENT_skipverification(GMAILAGENT_T ag, int yes) {
	assert(ag);
	int old = ag->skip_verification;
	ag->skip_verification = yes;
	return old;
}

int GMAILAGENT_auth(GMAILAGENT_T ag, char* username, 
                                    char* password) {
	assert(ag);
	assert(ag->arena);
	assert(username && *username);
	assert(password && *password);

	astrdup(ag->arena, ag->username, username);
	astrdup(ag->arena, ag->password, password);

	return 1;
}

int GMAILAGENT_from(GMAILAGENT_T ag, char* from) {
	assert(ag);
	assert(ag->arena);
	astrdup(ag->arena, ag->from, from);
	return 1;
}

int GMAILAGENT_to(GMAILAGENT_T ag, char* to) {
	assert(ag);
	assert(ag->arena);
	int i = 0;
	for (i=0; i<MAXTO-1 && ag->to[i] != 0; i++);
	astrdup(ag->arena, ag->to[i], to);
	return 1;
}

int GMAILAGENT_message(GMAILAGENT_T ag, char* msg) {
	assert(ag);
	assert(ag->arena);
	assert(msg);

	astrdup(ag->arena, ag->message, msg);
	return 1;
}

void GMAILAGENT_finish(GMAILAGENT_T ag) {
	assert(ag);
	assert(ag->arena);
	Arena_free(ag->arena);
	FREE(ag);
}
	
static size_t payload_source(void *ptr, size_t size, size_t nmemb, void *userdata) {
	size_t bufsz = size * nmemb;
	static size_t position = 0;

	dputs("uploading message at maxium: %d, %d", size, nmemb);
	if ((0 == size) || (0 == nmemb) || (1 > bufsz)) 
		return 0;

	char* data = (char *)userdata;
	if (data && *data != '\0') {
		size_t len = strlen(data);
		size_t remain = len - position;

		if (remain <= bufsz) { /* remain string shorter than bufsz */
			memcpy(ptr, data + position, remain);
			position = 0;
			*data = '\0';
			return remain;
		} else {
			memcpy(ptr, data + position, bufsz);
			position += bufsz;
			return bufsz;
		}
	}

	return 0;
}

int GMAILAGENT_send(GMAILAGENT_T ag) {
	assert(ag);
	assert(ag->arena);
	assert(ag->server);
	assert(ag->port);
	assert(ag->username);
	assert(ag->password);
	assert(ag->message);
	assert(ag->from);
	assert(ag->to);

	CURL *curl;
	CURLcode res = CURLE_OK;
	int exitcode = 1; /* true for success */
	int i = 0;

	curl = curl_easy_init();
	if (! curl) {
		return 0;
	}

	curl_easy_setopt(curl, CURLOPT_USERNAME, ag->username);
	curl_easy_setopt(curl, CURLOPT_PASSWORD, ag->password);
	curl_easy_setopt(curl, CURLOPT_URL, ag->server);
	if (ag->skip_verification) {
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
	}
	curl_easy_setopt(curl, CURLOPT_MAIL_FROM, ag->from);
	
	struct curl_slist *recipients = NULL;
	for (i = 0; i<MAXTO && ag->to[i]; i++) {
		recipients = curl_slist_append(recipients, ag->to[i]);
	}

	curl_easy_setopt(curl, CURLOPT_MAIL_RCPT, recipients);
	curl_easy_setopt(curl, CURLOPT_READFUNCTION, payload_source);
	curl_easy_setopt(curl, CURLOPT_READDATA, ag->message);
	curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
	if (debug)
		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

	res = curl_easy_perform(curl);
	if (res != CURLE_OK) {
		fprintf(stderr, "cannot send mail: %s\n", curl_easy_strerror(res));
		exitcode = 0;
	}
	curl_slist_free_all(recipients);
	curl_easy_cleanup(curl);
	return exitcode;
}

