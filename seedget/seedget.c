#include "defs.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <mrss.h>
#include <curl/curl.h>

#include <cii/assert.h>
#include <cii/mem.h>
#include <cii/set.h>

#include "storable_set.h"
#include "infohash.h"
#include "html_escape.h"
#include "gmailagent.h"
#include "telegram.h"
#include "debug.h"

int debug = 0;


#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define HASHLEN 40
typedef unsigned char bool;
	
static char* url = NULL;
static char* outputdir = ".";
static char* mailto = NULL;
static char* mailfrom = NULL;
static char* mailacct = NULL;
static char* mailpass = NULL;
static char* mailserv = NULL;
static int   skipverify = 0;
static char* patterns = NULL; /* commar seperated lists */
static char* subject = "You got notification";
static const char* delim = "|";
static char* token = NULL;
static char* chatid = NULL;


/* default user-agent */
/* "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0"); */
char* useragent = NULL;
char* cache = NULL;
char* cache_suffix = ".cache";

int mailport = 25;
int timeout = 20;
bool seedlink;
bool follow = FALSE;

#define TERMSTR(s) s[sizeof(s)-1] = '\0'
#include "alltests.h"
#define OPT(v) "--" #v

struct report_item {
	struct report_item* rest;
	char* title;
	char* link;
};
typedef struct report_item* report_item;
	
void usage() {
	fprintf(stderr, "%s %s\n", PACKAGE_NAME, PACKAGE_VERSION);
	fprintf(stderr, "Usage:\n");
	fprintf(stderr, "\t%-16s%s\n", OPT(url), "rss url");
	fprintf(stderr, "\t%-16s%s\n", OPT(cache), "downloading history file");
	fprintf(stderr, "\t%-16s%s\n", OPT(outputdir), "save seed file to this directory");
	fprintf(stderr, "\t%-16s%s\n", OPT(follow), "download torrent seed file also");
	fprintf(stderr, "\t%-16s%s\n", OPT(mailto), "mail recipients, '|' seperated list");
	fprintf(stderr, "\t%-16s%s\n", OPT(mailfrom), "mail from");
	fprintf(stderr, "\t%-16s%s\n", OPT(mailacct), "account");
	fprintf(stderr, "\t%-16s%s\n", OPT(mailpass), "password");
	fprintf(stderr, "\t%-16s%s\n", OPT(mailserv), "default smtp.gmail.com");
	fprintf(stderr, "\t%-16s%s\n", OPT(mailport), "default 465");
	fprintf(stderr, "\t%-16s%s\n", OPT(subject), "notify message's subject");
	fprintf(stderr, "\t%-16s%s\n", OPT(patterns), "commar seperated pattern to match topic");
	fprintf(stderr, "\t%-16s%s\n", OPT(skipverify), "skip ssl certificate verification");
	fprintf(stderr, "\t%-16s%s\n", OPT(useragent), "use this as http agent name");
	fprintf(stderr, "\t%-16s%s\n", OPT(token), "telegram bot's token");
	fprintf(stderr, "\t%-16s%s\n", OPT(chatid), "telegram bot's chat id");
	fprintf(stderr, "\t%-16s%s\n", OPT(version), "version number");
	fprintf(stderr, "\t%-16s%s\n", OPT(test), "do self tests");
	fprintf(stderr, "\t%-16s%s\n", OPT(debug), "output debug info too");
	fprintf(stderr, "\t%-16s%s\n", OPT(help), "this message");
}

static size_t header_callback(char *buffer, size_t size, size_t nitems, void *userdata) {
	/* received header is nitems * size long in 'buffer' NOT ZERO TERMINATED */
	/* 'userdata' is set with CURLOPT_HEADERDATA */
	size_t i;
	char header[] = "content-disposition:";
//	dputs("header_callback: size: %d nitems: %d", size, nitems);
//	dputs("header_callback: buffer: %s", buffer);
	if (0 == strncasecmp(header, buffer,  sizeof(header)-1)) {
		dputs("got file name in http header: %s", buffer);
		strncpy(userdata, buffer, 1024);
	}
	return nitems * size;
}

/*
static size_t body_callback(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
	assert(stream);
//	dputs("body_callback: size: %d nmemb: %d", size, nmemb);
	size_t written = fwrite(ptr, size, nmemb, stream);
	return written;
}
*/

/* download seed from link */
/* return: char* filename for success, NULL for fail */
char* download(char *l) {
	CURL *curl;
	CURLcode res;

	char content_disposition_header[1024] = {'\0'};
	char *filename = NULL;
	int len = asprintf(&filename, "%s%s", outputdir, "/" PACKAGE_NAME "-XXXXXX");
	assert(len>0);
	assert(filename);
	int fd = mkstemp(filename);
	assert(fd);

	/* destination file if filename is in content disposition header */
	char *destfile = NULL;
	dputs("use %s as temporary file name", filename);

	FILE *fp = fdopen(fd, "w");
	if (fp == NULL) {
		perror("cannot write file");
		exit(1);
	}

	curl = curl_easy_init();
	if(curl) {
		curl_easy_setopt(curl, CURLOPT_URL, l);

		  /* complete default within 20 seconds */
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeout);

		if (useragent)  {
			curl_easy_setopt(curl, CURLOPT_USERAGENT, useragent);
		}

		/* example.com is redirected, so we tell libcurl to follow redirection */ 
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, header_callback);
		curl_easy_setopt(curl, CURLOPT_HEADERDATA, content_disposition_header);
//		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, body_callback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

		/* Perform the request, res will get the return code */ 
		res = curl_easy_perform(curl);

		/* Check for errors */ 
		if(res != CURLE_OK)
			fprintf(stderr, "cannot download link: %s\n", curl_easy_strerror(res));

		/* always cleanup */ 
		curl_easy_cleanup(curl);

	} 

	fclose(fp);

	if (res != CURLE_OK) {
		/* remove temporary file */
		if ( 0 > remove(filename) ) {
			perror("cannot remove temporary file");
			exit(1);
		}
		return NULL;
	}


	if ( '\0' != *content_disposition_header) {
		char *p = content_disposition_header;

		 // Content-Disposition: attachment; filename="file.torrent"
		if (0 == strncasecmp(content_disposition_header, 
		                     "Content-Disposition: attachment; filename=", 42)) {
			p = content_disposition_header + 42; /* skip " , p point to file name start character */
			if ('"' == *p) p++;

			char *q;
			q = strchr(p, '"');
			if (q) {
				*q = '\0';
			}

			p = html_unescape(p);

			dputs("save download to file: %s", p);

			int destlen = strlen(p) + strlen(outputdir) + 2; /* include '/' and '\0' */
			destfile = ALLOC(destlen);
			*destfile = '\0';

			strcat(destfile, outputdir);
			strcat(destfile, "/");
			strcat(destfile, p);
			FREE(p);

			if ( 0 > rename(filename, destfile)) {
				perror("cannot rename file");
				assert(0);
			}
			free(filename);
			return destfile;
		}

	} 
		
	return filename;
}

/* commar/zero ending string length */
int cstrlen(char *s) {
	assert(s);
	char* b = s;
	while (*s != ',' && *s != '\0')
		s++;
	return s-b;
}	

/* check the input string
 * return true if input match patterns */
int filter(char* patterns, const char* s) {
	assert(patterns);
	assert(s);

	static regex_t regex;
	static char *last_pattern = NULL;

	int reti;
	char buf[BUFSIZ];

	if (last_pattern != patterns) {
		if (last_pattern != NULL) regfree (&regex);
		reti = regcomp (&regex, patterns, 0);
		if (reti) {
			fprintf (stderr, "Could not compile regex\n");
			exit (1);
		}
		last_pattern = patterns;
	}

	reti = regexec (&regex, s, 0, NULL, 0);
	return !reti;
}

	

int main(int argc, char* argv[]) {
	int i;
	report_item report = NULL;

	for (i=1; i<argc; i++) {
		if ( 0 == strcmp(argv[i], OPT(url))) {
			url = argv[++i];
		} else if ( 0 == strcmp(argv[i], OPT(outputdir))) {
			outputdir = argv[++i];
		} else if ( 0 == strcmp(argv[i], OPT(cache))) {
			cache = argv[++i];
		} else if ( 0 == strcmp(argv[i], OPT(subject))) {
			subject = argv[++i];
		} else if ( 0 == strcmp(argv[i], OPT(mailto))) {
			mailto = argv[++i];
		} else if ( 0 == strcmp(argv[i], OPT(mailfrom))) {
			mailfrom = argv[++i];
		} else if ( 0 == strcmp(argv[i], OPT(mailacct))) {
			mailacct = argv[++i];
		} else if ( 0 == strcmp(argv[i], OPT(mailpass))) {
			mailpass = argv[++i];
		} else if ( 0 == strcmp(argv[i], OPT(useragent))) {
			useragent = argv[++i];
		} else if ( 0 == strcmp(argv[i], OPT(mailserv))) {
			mailserv = argv[++i];
		} else if ( 0 == strcmp(argv[i], OPT(mailport))) {
			mailport = atoi(argv[++i]);
			if (mailport < 0 || mailport > 65535) {
				fprintf(stderr, "wrong port number");
				exit(1);
			}
		} else if ( 0 == strcmp(argv[i], OPT(skipverify))) {
			skipverify = 1;
		} else if ( 0 == strcmp(argv[i], OPT(patterns))) {
			patterns = argv[++i];
		} else if ( 0 == strcmp(argv[i], OPT(token))) {
			token = argv[++i];
		} else if ( 0 == strcmp(argv[i], OPT(chatid))) {
			chatid = argv[++i];
		} else if ( 0 == strcmp(argv[i], OPT(timeout))) {
			timeout = atoi(argv[++i]);
			if (timeout < 0 || timeout > 32767) {
				fprintf(stderr, "wrong timeout number");
				exit(1);
			}
		} else if ( 0 == strcmp(argv[i], OPT(debug))) {
			debug = 1;
		} else if ( 0 == strcmp(argv[i], OPT(follow))) {
			follow = TRUE;
		} else if ( 0 == strcmp(argv[i], OPT(test))) {
			RunAllTests();
			exit(0);
		} else if ( 0 == strcmp(argv[i], OPT(help))) {
			usage();
			exit(0);
		} else if ( 0 == strcmp(argv[i], OPT(version))) {
			fprintf(stderr, "%s\n", PACKAGE_VERSION);
			exit(0);
		} else {
			fprintf(stderr, "wrong argument: %s\n", argv[i]);
			usage();
			exit(1);
		}
	}
	if (!cache) {
		int l = strlen(argv[0]) + strlen(cache_suffix) + 1;
		cache = ALLOC(l);
		*cache = '\0';
		strcat(strcat(cache, argv[0]), cache_suffix);
	}

	prvar("%s", url);
	prvar("%s", cache);
	prvar("%s", useragent);
	prvar("%d", follow);
	prvar("%s", outputdir);
	prvar("%s", mailserv);
	prvar("%d", mailport);
	prvar("%s", mailfrom);
	prvar("%s", mailto);
	prvar("%s", mailacct);
	prvar("%s", mailpass);
	prvar("%s", token);
	prvar("%s", chatid);
	prvar("%s", patterns);
	prvar("%d", skipverify);
	prvar("%d", timeout);
	prvar("%d", debug);

	Set_T cch = retrieve(cache);

	mrss_t* data = NULL;
	mrss_error_t ret;
	mrss_item_t* item = NULL;
	CURLcode code;

	if (NULL == url) {
		usage();
		exit(1);
	}

	if (!strncmp (url, "http://", 7) || !strncmp (url, "https://", 8)) {
		mrss_options_t *mrssopt = NULL;
		if (useragent || timeout) {
			mrssopt = mrss_options_new (
					timeout ? timeout : 30,
					NULL, /* proxy */
					NULL, /* proxy auth */
					NULL, /* cert */
					NULL, /* cert password */
					NULL, /* ca cert */
					0, /* not verify peer */
					NULL, /* authorication, like usr:pwd */
					useragent /* user agent for http client */
					);
			assert(mrssopt);
		}
		ret = mrss_parse_url_with_options_and_error (url, &data, mrssopt, &code);
		mrss_options_free(mrssopt);
	}
	else
		ret = mrss_parse_file (url, &data);


	if (ret) {
		fprintf(stderr, "MRSS return error: %s\n",
				ret ==
				MRSS_ERR_DOWNLOAD ? mrss_curl_strerror (code) :
				mrss_strerror (ret));
		return 1;
	}

	for (item = data->item; item; item=item->next) {
		/* filter on topic */
		if (patterns && ! filter(patterns, item->title)) {
			continue;
		}

		if (item->enclosure_url) {
			char* seedhash = NULL;
			char* p = item->enclosure_url;
			int urllen = strlen(p);
			if (urllen < HASHLEN) {
				dputs("wrong enclosure url %s", p);
				continue;
			}

			/* assume hash is at end of enclosure url*/
			seedhash = p + urllen - HASHLEN;
			dputs("found seed hash: %s", seedhash);

			if (Set_member(cch, seedhash)) { /* exist already */
				dputs("cache hit on %s", seedhash);
			} else {
				dputs("cache miss on %s", seedhash);
				if (follow) {

					/* follow enclosure url */
					dputs("download %s", item->enclosure_url);
					char* seed = download(item->enclosure_url);
					if (NULL == seed) continue;

					char sha1[43] = { '\0' };
					if (info_sha1(seed, sha1)){
						if (0 == strcmp(seedhash, sha1)) {
							Set_put(cch, seedhash);
						} else {
							/* wrong seed file */
							fprintf(stderr, "wrong info hash on download file:\n\texpect: %s\n\t   got: %s", seedhash, sha1);
							remove(seed);
							continue;
						}
					} else {
						fprintf(stderr, "calculate info hash failed on file: %s", seed);
						remove(seed);
						continue;
					}
				} else {
					/* print out enclosure url */
					printf("%s|%s|%s\n", item->title, item->link, item->enclosure_url);
					Set_put(cch, seedhash);
				}

				report_item r = NEW0(r);
				r->link = strdup(item->link);
				r->title = strdup(item->title);
				r->rest = report;
				report = r;
			}
		}
	}


	int len;
	char *message = NULL;
	char* tmpmsg=NULL;
	if (report) { /* send notification */
		/* send notify mail */
		if (mailto &&  mailfrom &&  mailacct && mailpass) {
			dputs("sending gmail ...");


			/* format mail message */
			len = asprintf(&message, "Subject: %s\r\nContent-Type: text/html; charset=UTF-8\r\n\r\n", subject);
			assert(len>0);

			report_item r = report;
			while (r) {
				len = asprintf(&tmpmsg, "%s<p><a href=\"%s\">%s</a>\r\n", message, r->link, r->title);
				free(message);
				assert(len>0);
				message=tmpmsg;
				r = r->rest;
			}

			tmpmsg = message;
			message = html_unescape(tmpmsg);
			free(tmpmsg);
			dputs("mail body:\n%s", message);

			GMAILAGENT_T gmail;
			gmail = GMAILAGENT_new();
			GMAILAGENT_auth(gmail, mailacct, mailpass);
			GMAILAGENT_from(gmail, mailfrom);
			GMAILAGENT_to(gmail, mailto);
			GMAILAGENT_skipverification(gmail, skipverify);
			GMAILAGENT_message(gmail, message);
			GMAILAGENT_send(gmail);
			GMAILAGENT_finish(gmail);
			free(message);
		}

		/* send notify by Telegram instant message */
		if (token && chatid) {
			len = asprintf(&message, "%s\n\n", subject);
			assert(len>0);

			report_item r = report;
			while (r) {
				len = asprintf(&tmpmsg, "%s%s\n%s\n\n", message, r->title, r->link);
				free(message);
				assert(len>0);
				message=tmpmsg;
				r = r->rest;
			}

			tmpmsg = message;
			message = html_unescape(tmpmsg);
			free(tmpmsg);

			Telegram_send(token, chatid, message);
			free(message);
		}
	}

	/* save cache */
	store(cch, cache);
	mrss_free(data);
}
