#ifndef STORABLE_INCLUDED
#define STORABLE_INCLUDED
extern Set_T retrieve(const char *); 
extern int store(Set_T, const char *);
#endif

