#ifndef HTML_ESCAPE_INCLUDED
#define HTML_ESCAPE_INCLUDED
#include <cii/except.h>
extern char* html_escape(char* src);
extern char* html_unescape(char* src);
extern Except_T HTML_ESCAPE_Failed;
#endif
