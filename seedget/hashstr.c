#include <string.h>
#include "hashstr.h"

/* Use folding on a string, summed 4 bytes at a time */
unsigned hashstr(const void *key) {
	int len = strlen(key);
	int nint = len / sizeof(int);
	const int *pint = key;
	unsigned int sum = 0;

	int j;
	for (j = 0; j < nint; j++) {
		sum += *pint++;
	}

	for (j = nint * sizeof(int); j < len; j++) {
		sum += *(char*)(key + j);
	}
	return(sum);
}

/* Unit tests */
#include "cutest/CuTest.h"

void TestHashstr_a(CuTest *tc) {
	CuAssertIntEquals(tc, 0x61, hashstr("a"));
}

void TestHashstr_ab(CuTest *tc) {
	CuAssertIntEquals(tc, 0x61+0x62, hashstr("ab"));
}

void TestHashstr_abcd(CuTest *tc) {
	CuAssertIntEquals(tc, 0x64636261, hashstr("abcd"));
}

