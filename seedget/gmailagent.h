#ifndef GMAILAGENT_INCLUDED
#define GMAILAGENT_INCLUDED
typedef struct GMAILAGENT_T *GMAILAGENT_T;

struct GMAILAGENT_T;

struct Except_T GMAILAGENT_FAIL;

GMAILAGENT_T GMAILAGENT_new(void);
int GMAILAGENT_auth(GMAILAGENT_T ag, char* username, char* password);
int GMAILAGENT_from(GMAILAGENT_T ag, char* from);
int GMAILAGENT_to(GMAILAGENT_T ag, char* to);
int GMAILAGENT_message(GMAILAGENT_T ag, char* msg);
int GMAILAGENT_send(GMAILAGENT_T ag);
int GMAILAGENT_skipverification(GMAILAGENT_T ag, int yes);
void GMAILAGENT_finish(GMAILAGENT_T ag);

#endif
