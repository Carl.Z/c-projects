#include "defs.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cii/assert.h>
#include <cii/mem.h>
#include <curl/curl.h>
#include "telegram.h"
#include "debug.h"

/* send message to telegram server
 * need token and chat id provided
 * return 1 when success
 * return 0 when failure
*/

static const char *telegram_url = "https://api.telegram.org/bot";
extern int validutf8(const char*, unsigned char*);

int Telegram_send(char *token, char *chatid, char *message) {
	assert(token);
	assert(*token);
	assert(chatid);
	assert(*chatid);
	assert(message);

	if ('\0' == *message) {
		return 1; /* empty message */
	}

	/* valid message a well formated utf8 */
	unsigned char ch;
	dputs("telegram send message: length=%d", strlen(message));
	int offset = validutf8(message, &ch);
	if (offset != -1) {
		fprintf(stderr, "wrong character found at offset: %d, char: %d\n", offset, ch);
		fprintf(stderr, "messages:\n\n%s", message);
		exit(EXIT_FAILURE);
	}

	int toklen = strlen(token);
	char* command = "sendMessage";

	char* request = NULL;
	asprintf(&request, "%s%s/%s", telegram_url, token, command);
	assert(request);

	CURL *curl;
	CURLcode res;

	curl = curl_easy_init();
	assert(curl);
	
	char* postdata = NULL;
	char* message_enc = curl_easy_escape(curl, message, 0L);
	asprintf(&postdata, "chat_id=%s&text=%s", chatid, message_enc);
	assert(postdata);
	curl_free(message_enc);

	curl_easy_setopt(curl, CURLOPT_URL, request);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postdata);

	if (debug)
		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
	else {
		size_t dummywrite(void *ptr, size_t size, size_t nmemb, void* stream) { return size * nmemb; }
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, dummywrite);
	}

	dputs("send message to %s", request);
	dputs("message:\n%s", postdata);
	res = curl_easy_perform(curl);
	if (res != CURLE_OK) 
	      fprintf(stderr, "failed to send message to telegram: %s\n", curl_easy_strerror(res));
	else if (debug)
		dputs("successfully sent");
	
	/* always cleanup */ 
	curl_easy_cleanup(curl);
	free(request);
	free(postdata);
}
